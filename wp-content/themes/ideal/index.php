<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>
	
</div>
</div>
	
	<div id="fundoBanner"> 
		<div id="wrapper" class="container">
			<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
				<div class="cycle-slideshow" id="banner_img"
				data-cycle-fx=scrollhorz
				data-cycle-timeout=2000
				data-cycle-slides="> div"
    			data-cycle-pause-on-hover="true"
				>
					<?php $query = new WP_Query(array(
					    'post_type' => 'banner',
					    'posts_per_page' => 2,
					));?><?php
								if ($query->have_posts()) {
								 	while ($query->have_posts()) {
								 		$query->the_post();
								 		?>
								 		<div id="divTeste" style="background:<?php the_field('background_color'); ?>">
								 			<a href="<?php the_field('url_banner'); ?>">
								 				<?php the_post_thumbnail(); ?>
								 			</a>
								 		</div>
								 		 <?php
								 	}
								 } 
							?>	
				</div>
			</main>
		</div>
	</div>

	<div class="traco" id="marginTopMarginBottom">
		<div id="wrapper" class="container">
			<div class="row">
				<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
					<div id="titulo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
						<h3>Artigos</h3>
					</div>
				</main>
			</div>
		</div>
	</div>
	<div id="wrapper" class="container">
	<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="artigos">
				<?php $query = new WP_Query(array(
					'post_type' => 'artigos',
					'posts_per_page' => 4,
				));
				if ($query->have_posts()) {
					while ($query->have_posts()) {
						$query->the_post();
						?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<a href="<?php the_permalink(); ?>" id="artigoIdeal">
							<?php the_post_thumbnail(); ?>
							<h4><?php the_title();?></h4>
							</a>
						</div><?php
									}
								} 
							?>	
			</div>
	</main>		
	</div>
	<div class="traco" id="marginTop">
		<div id="wrapper" class="container">
			<div class="row">
				<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
					<div id="titulo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
						<h3>Parceiros</h3>
					</div>
				</main>
			</div>
		</div>
	</div>
	<div id="wrapper" class="container">
	<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="parceiros">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-6 col-xs-6" id="parceiros1">
						<?php $query = new WP_Query(array(
						    'post_type' => 'parceiros',
						    'posts_per_page' => 5,
						    'category__in' => 2,
						));
							if ($query->have_posts()) {
								while ($query->have_posts()) {
									$query->the_post();
									?>
									 	<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<?php the_post_thumbnail(); ?>
									 		<!-- <img src="<?php the_field('imagem_antes'); ?>"> -->
									 	</div><?php
									}
								} 
							?>	
					</div>
					<div class="col-lg-12 col-md-12 col-sm-6 col-xs-6" id="parceiros2">
						<?php $query = new WP_Query(array(
						    'post_type' => 'parceiros',
						    'posts_per_page' => 5,
						    'category__in' => 3,
						));
							if ($query->have_posts()) {
								while ($query->have_posts()) {
									$query->the_post();
									?>
									 	<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<?php the_post_thumbnail(); ?>
									 		<!-- <img src="<?php the_field('imagem_antes'); ?>"> -->
									 	</div><?php
									}
								} 
							?>	
					</div>
				</div>
			</div>
					
				
		</div>
	</main><!-- #content -->

<?php
get_footer();

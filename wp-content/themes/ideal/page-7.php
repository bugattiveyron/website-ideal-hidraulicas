<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

	</div>
	</div>
	<div class="traco" id="marginBottom">
		<div id="wrapper" class="container">
			<div class="row">
				<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
					<div id="titulo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
						<h3><?php the_title();?></h3>
					</div>
				</main>
			</div>
		</div>
	</div>

		<div id="wrapper" class="container">
			<div class="row">
	<main id="content" class="<?php echo odin_classes_page_full(); ?>" tabindex="-1" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
				?>
				<div id="formularioContato" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<h3>Preencha o formulario abaixo:</h3>
					<?php the_content();?>
				</div>			
				<div id="mapaContato" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<iframe frameborder="0" style="width: 100%; height:400px; overflow: hidden; border: none;" src="http://maps.google.com/maps?hl=en&amp;f=q&amp;source=s_q&amp;geocode=&amp;q=Rua Tancredo de Almeida Neves, 221 - Extrema/MG&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Rua Tancredo de Almeida Neves, 221 - Extrema/MG&amp;z=15&amp;iwloc=addr&amp;output=embed"></iframe>
				</div>
<?php
					// If comments are open or we have at least one comment, load up the comment template.
					/*if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;*/
				endwhile;
			?>

	</main><!-- #main -->

<?php
get_footer();

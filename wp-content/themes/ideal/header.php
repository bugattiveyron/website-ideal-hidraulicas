<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Odin
 * @since 2.2.0
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" rel="shortcut icon" />
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.js"></script>
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
	
	<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/jquery.cycle2.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/cycle.js" type="text/javascript"></script>
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>	
<div id="fb-root"></div>

</head>
	<script type="text/javascript">
		function teste(valor){
			
			alert(valor);
			
			/*document.getElementById('fundoBanner').style.background = valor;*/
		}
	</script>
<body <?php body_class(); ?>>
	<a id="skippy" class="sr-only sr-only-focusable" href="#content"><div class="container"><span class="skiplink-text"><?php _e( 'Skip to content', 'odin' ); ?></span></div></a>
	<header id="header" role="banner">
		<section id="contatoHeader">
			<div class="container">
				<article id="endereco" class="col-lg-8 col-md-10 col-sm-12">
					<img id="imagemEndereco" src="<?php echo get_template_directory_uri();?>/assets/images/iconLocal.png">
					<p id="labelEndereco">Avenida Tancredo de Almeida Neves, 221 - Extrema/MG</p>
				
					<img id="telfoneHeader" src="<?php echo get_template_directory_uri();?>/assets/images/iconTelefone.png">
					<p id="labelTelefone">(35) 3435-2603</p>
				
					<img id="imagemEmail" src="<?php echo get_template_directory_uri();?>/assets/images/iconEmail.png">
					<p id="labelEmail">idealhidraulica@gmail.com</p>
				
					<img id="iconFacebook" src="<?php echo get_template_directory_uri();?>/assets/images/iconFacebook.png">

				</article>
			</div>
		</section>
		<div class="container">

			<div class="page-header col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div id="logo">
						<a href="http://www.idealsolucoeshidraulicas.com.br/" title="VOLTAR PARA HOME"><img src="<?php echo get_template_directory_uri();?>/assets/images/logoHeader.png"></a>
					</div>
				
			</div><!-- .site-header-->
	
			<div class="semBorda col-lg-offset-3 col-lg-5 col-md-offset-2 col-md-6 col-sm-offset-2 col-sm-8">
				<div id="main-navigation" class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-navigation">
						<span class="sr-only"><?php _e( 'Toggle navigation', 'odin' ); ?></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- <a class="navbar-brand visible-xs-block" href="<?php echo home_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> -->
					</div>
					<nav class="collapse navbar-collapse navbar-main-navigation" role="navigation">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'main-menu',
									'depth'          => 2,
									'container'      => false,
									'menu_class'     => 'nav navbar-nav',
									'fallback_cb'    => 'Odin_Bootstrap_Nav_Walker::fallback',
									'walker'         => new Odin_Bootstrap_Nav_Walker()
								)
							);
						?>
						<!-- <form method="get" class="navbar-form navbar-right" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
							<label for="navbar-search" class="sr-only"><?php _e( 'Search:', 'odin' ); ?></label>
							<div class="form-group">
								<input type="search" class="form-control" name="s" id="navbar-search" />
							</div>
							<button type="submit" class="btn btn-default"><?php _e( 'Search', 'odin' ); ?></button>
						</form> -->
					</nav><!-- .navbar-collapse -->
				</div><!-- #main-navigation-->
			</div>

		</div><!-- .container-->
	</header><!-- #header -->

	<div id="wrapper" class="container">
		<div class="row">

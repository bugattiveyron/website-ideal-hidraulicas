<?php
/**
 * The template for displaying a "No posts found" message.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
</div>
</div>
<div class="traco">
		<div id="wrapper" class="container">
			<div class="row">
				<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
					<div id="titulo" class="col-lg-9 col-md-8 col-sm-7 col-xs-12">	
						<h3><?php _e( 'Nothing Found', 'odin' ); ?></h3>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12" id="botaoPesquisa">
						<form method="get" class="navbar-form navbar-right" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
							<label for="navbar-search" class="sr-only"><?php _e( 'Search:', 'odin' ); ?></label>
							<div class="form-group">
								<button type="submit" class="btn btn-default pesquisa"><img src="<?php echo get_template_directory_uri();?>/assets/images/lupa.png"></button><input type="search" class="form-control" name="s" id="navbar-search" placeholder="Buscar..."/>
							</div>
							
						</form>
					</div>
				</main>
			</div>
		</div>
	</div>

		<div id="wrapper" class="container">
			<div class="row">
<header class="page-header">

	
</header>
<div class="col-lg-9 col-md-8 col-sm-7 col-xs-12">
<div class="page-content">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

		<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'odin' ), admin_url( 'post-new.php' ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

		
		
		<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'odin' ); ?></p>
		
				

	<?php else : ?>
		
		<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'odin' ); ?></p>

	<?php endif; ?>
</div><!-- .page-content -->
</div>
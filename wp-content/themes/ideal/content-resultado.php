<?php
/**
 * The default template for displaying content.
 *
 * Used for both single and index/archive/author/catagory/search/tag.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
	
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="" id="postArtigos">
							 	<a href="<?php the_permalink(); ?>"><h4><?php the_title();?></h4></a>
							 	<a href="<?php the_permalink(); ?>" id="postBlogIMG" class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
			 		 					<?php the_post_thumbnail(array('alt' => ''.get_the_title().'')); ?>
							 	</a>
							 	<a href="<?php the_permalink(); ?>" id="postBlog">
								 	<div class="row">
										<p><?php the_excerpt_max_charlength(550) ?></p>
									</div>
								</a>
							</div>
</article><!-- #post-## -->

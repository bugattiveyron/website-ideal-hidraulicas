<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

	<main id="content" class="<?php echo odin_classes_page_full(); ?>" tabindex="-1" role="main">		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="paginaParceiros">
					<h3><?php the_title();?></h3>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="paginaParceiros1">
						<?php $query = new WP_Query(array(
						    'post_type' => 'parceiros',
						    'posts_per_page' => 5,
						    'category__in' => 3,
						));
						if ($query->have_posts()) {
							while ($query->have_posts()) {
								$query->the_post();
								?>
								<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
									<a target="_blank" href="<?php the_field('link');?>"><img src="<?php the_field('imagem_antes'); ?>"></a>
								</div><?php
							}
						} 
						?>	
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="paginaParceiros2">
						<?php $query = new WP_Query(array(
						    'post_type' => 'parceiros',
						    'posts_per_page' => 5,
						    'category__in' => 4,
						));
						if ($query->have_posts()) {
							while ($query->have_posts()) {
								$query->the_post();
								?>
								<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
									
										<a href="<?php the_field('link');?>"><img src="<?php the_field('imagem_antes'); ?>"></a>
									
										
								</div><?php
								}
						} 
						?>	
					</div>
				</div>
			</div>
		</div>	
		<div class="cycle-slideshow col-lg-offset-1 col-lg-10" id="banner_imgParceiros"
		data-cycle-fx=scrollhorz
		data-cycle-timeout=3000
		data-cycle-slides="> div"
		>
			<?php $query = new WP_Query(array(
			    'post_type' => 'banner',
			    'posts_per_page' => 7,
			));
						if ($query->have_posts()) {
						 	while ($query->have_posts()) {
						 		$query->the_post();
						 		?>
						 		<div>
						 		 <?php the_post_thumbnail(); ?>
						 		</div>
						 		 <?php
						 	}
						 } 
					?>	
		</div>
		<div class="cycle-slideshow col-lg-offset-1 col-lg-10" id="banner_imgParceiros"
				data-cycle-fx=scrollhorz
				data-cycle-timeout=3000
				data-cycle-slides="> div"
				>
					<?php $query = new WP_Query(array(
					    'post_type' => 'banner',
					    'posts_per_page' => 7,
					));?><?php
								if ($query->have_posts()) {
								 	while ($query->have_posts()) {
								 		$query->the_post();
								 		?>
								 		<div >

								 			 <?php the_post_thumbnail(); ?>
								 		</div>
								 		 <?php
								 	}
								 } 
							?>	
				</div>
	</main><!-- #main -->

<?php
get_footer();

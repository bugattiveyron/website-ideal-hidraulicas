<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

	
			<?php if ( have_posts() ) : ?>
</div>
</div>
				<div class="traco">
		<div id="wrapper" class="container">
			<div class="row">
				<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
					<div id="titulo" class="col-lg-9 col-md-8 col-sm-7 col-xs-12">	
						<h3>Resultados: <?php the_title();?></h3>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12" id="botaoPesquisa">
						<form method="get" class="navbar-form navbar-right" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
							<label for="navbar-search" class="sr-only"></label>
							<div class="form-group">
								<button type="submit" class="btn btn-default pesquisa"><img src="<?php echo get_template_directory_uri();?>/assets/images/lupa.png"></button><input type="search" class="form-control" name="s" id="navbar-search" placeholder="Buscar..."/>
							</div>
							
						</form>
					</div>
				</main>
			</div>
		</div>
	</div>
	<div id="wrapper" class="container">
			<div class="row">
<main id="content" class="<?php echo odin_classes_page_sidebar(); ?>" tabindex="-1" role="main">
					<?php
						// Start the Loop.
						while ( have_posts() ) : the_post();

							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', 'resultado' );

						endwhile;

						// Post navigation.
						odin_paging_nav();

					else :
						// If no content, include the "No posts found" template.
						get_template_part( 'content', 'none' );

				endif;
			?>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();

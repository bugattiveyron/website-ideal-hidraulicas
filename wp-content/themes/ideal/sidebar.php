<?php
/**
 * The sidebar containing the secondary widget area, displays on homepage, archives and posts.
 *
 * If no active widgets in this sidebar, it will shows Recent Posts, Archives and Tag Cloud widgets.
 *
 * @package Odin
 * @since 2.2.0
 */
?>

<aside id="sidebar" class="col-lg-3 col-md-4 col-sm-5 col-xs-12" role="complementary">
	<div class="fb-page" data-href="https://www.facebook.com/IdealHidraulicas" data-width="Min. 100%" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
		<div class="fb-xfbml-parse-ignore">
			<blockquote cite="https://www.facebook.com/IdealHidraulicas">
				<a href="https://www.facebook.com/IdealHidraulicas">Ideal Soluções Hidráulicas</a>
			</blockquote>
		</div>
	</div>
</aside><!-- #sidebar -->

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>
	</div>
	</div>
	<div class="traco" id="marginBottom">
		<div id="wrapper" class="container">
			<div class="row">
				<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
					<div id="titulo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
						<h3><?php the_title();?></h3>
					</div>
				</main>
			</div>
		</div>
	</div>

		<div id="wrapper" class="container">
			<div class="row">
	<main id="content" class="<?php echo odin_classes_page_full(); ?>" tabindex="-1" role="main">		
		<div class="row">
			<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="paginaParceiros">
					<h3><?php the_title();?></h3>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="paginaParceiros1">
						<?php $query = new WP_Query(array(
						    'post_type' => 'parceiros',
						    'posts_per_page' => 5,
						    'category__in' => 3,
						));
						if ($query->have_posts()) {
							while ($query->have_posts()) {
								$query->the_post();
								?>
								<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
									<a href="<?php the_field('link');?>"><img src="<?php the_field('imagem_antes'); ?>"></a>
								</div><?php
							}
						} 
						?>	
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="paginaParceiros2">
						<?php $query = new WP_Query(array(
						    'post_type' => 'parceiros',
						    'posts_per_page' => 5,
						    'category__in' => 4,
						));
						if ($query->have_posts()) {
							while ($query->have_posts()) {
								$query->the_post();
								?>
								<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
									
										<a href="<?php the_field('link');?>"><img src="<?php the_field('imagem_antes'); ?>"></a>
									
										
								</div><?php
								}
						} 
						?>	
					</div>
				</div>
			</div> -->
			<?php $query = new WP_Query(array(
			    'page_id' => '5',
			));
				if ($query->have_posts()) {
						 	while ($query->have_posts()) {
						 		$query->the_post();
						 		?>
						 		<div id="imagensParceiros">
						 			<?php the_content(); ?>
						 		</div>
						 		<?php
						 	}
						 }
			?>
		</div>	
		<div class="cycle-slideshow col-lg-offset-1 col-lg-10" id="banner_imgParceiros"
		data-cycle-fx=scrollhorz
		data-cycle-timeout=0
		data-cycle-slides="> div"
		>
			<?php $query = new WP_Query(array(
			    'post_type' => 'banner',
			    'posts_per_page' => 2,
			));
						if ($query->have_posts()) {
						 	while ($query->have_posts()) {
						 		$query->the_post();
						 		?>
						 		<div>
						 		<!--  <img id="backgroundBanner" src="<?php the_field('background_imagem'); ?>" style="background-repeat:repeat-x"> -->
						 		 <?php the_post_thumbnail(); ?>
						 		</div>
						 		 <?php
						 	}
						 } 
					?>	
		</div>
	</main><!-- #main -->

<?php
get_footer();

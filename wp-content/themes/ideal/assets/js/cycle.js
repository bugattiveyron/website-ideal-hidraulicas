jQuery(document).ready(function() {
jQuery( '.cycle-slideshow' ).on( 'cycle-initialized', function( event, opts ) {
	var img = jQuery( ".cycle-slide-active" ).attr( "src" );
    	jQuery("#imagens1").css( {
      		"background-image": "url("+img+")",
      		"background-position" : "left",
      		"background-repeat" : "repeat",
      		"-webkit-filter": "blur(8px)",
      		"-moz-filter": "blur(8px)",
      		"-o-filter": "blur(8px)",
      		"-ms-filter": "blur(8px)",
      		"filter": "blur(8px)"
    	} );
    	jQuery("#imagens2").css( {
      		"background-image": "url("+img+")",
      		"background-position" : "right",
      		"background-repeat" : "repeat",
      		"-webkit-filter": "blur(8px)",
      		"-moz-filter": "blur(8px)",
      		"-o-filter": "blur(8px)",
      		"-ms-filter": "blur(8px)",
      		"filter": "blur(8px)"
    	} );
});

	jQuery( '.cycle-slideshow' ).on( 'cycle-after', function( event, opts ) {
		var imagem = jQuery( ".cycle-slide-active" ).attr( "src" );
    	jQuery("#imagens1").css( {
      		"background-image": "url("+imagem+")",
      		"background-position" : "left",
      		"background-repeat" : "repeat",
      		"-webkit-filter": "blur(8px)",
      		"-moz-filter": "blur(8px)",
      		"-o-filter": "blur(8px)",
      		"-ms-filter": "blur(8px)",
      		"filter": "blur(8px)"
    	} );
    	jQuery("#imagens2").css( {
      		"background-image": "url("+imagem+")",
      		"background-position" : "right",
      		"background-repeat" : "repeat",
      		"-webkit-filter": "blur(8px)",
      		"-moz-filter": "blur(8px)",
      		"-o-filter": "blur(8px)",
      		"-ms-filter": "blur(8px)",
      		"filter": "blur(8px)"
    	} );
	});
});
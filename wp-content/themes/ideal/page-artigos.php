<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>
	</div>
	</div>
	<div class="traco">
		<div id="wrapper" class="container">
			<div class="row">
				<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
					<div id="titulo" class="col-lg-9 col-md-8 col-sm-7 col-xs-12">	
						<h3><?php the_title();?></h3>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12" id="botaoPesquisa">
						<form method="get" class="navbar-form navbar-right" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
							<label for="navbar-search" class="sr-only"><?php _e( 'Search:', 'odin' ); ?></label>
							<div class="form-group">
								<button type="submit" class="btn btn-default pesquisa"><img src="<?php echo get_template_directory_uri();?>/assets/images/lupa.png"></button><input type="search" class="form-control" name="s" id="navbar-search" placeholder="Buscar..."/>
							</div>
							
						</form>
					</div>
				</main>
			</div>
		</div>
	</div>
<div id="wrapper" class="container">
	<div class="row">
	<main id="content" class="col-lg-9 col-md-8 col-sm-7 col-xs-12" tabindex="-1" role="main">		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="">
				<?php $query = new WP_Query(array(
				    'post_type' => 'artigos',
				));
					if ($query->have_posts()) {
						while ($query->have_posts()) {
							$query->the_post();
							?>
							<div class="" id="postArtigos">
							 	<a href="<?php the_permalink(); ?>"><h4><?php the_title();?></h4>
							 	<div id="redesSociais"> 
								 	<div class="fb-like" data-share="true" data-href="https://www.facebook.com/IdealHidraulicas/" data-show-faces="false" data-layout="button"></div>
							 	</div></a>
							 	
							 	<a href="<?php the_permalink(); ?>" id="postBlogIMG" class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
			 		 					<?php the_post_thumbnail(array('alt' => ''.get_the_title().'')); ?>
							 	</a>
							 	<a href="<?php the_permalink(); ?>" id="postBlog">
								 	<div class="row">
										<p><?php the_excerpt_max_charlength(550) ?></p>
									</div>
								</a>
							</div><?php
						}
					} 
				?>	
			</div>
		</div>
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();

<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main div element.
 *
 * @package Odin
 * @since 2.2.0
 */
?>

		</div><!-- .row -->
	</div><!-- #wrapper -->

	<footer id="footer" role="contentinfo">
		<div class="container" id="informacoesRodape">
				<div id="menuRodape" class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
					<nav id="menu-rodape" class="main-rodape" role="navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'extra-menu', 'container_class' => 'my_extra_menu_class' ) ); ?>
					</nav>
				</div>
				<div id="logoIdeal" class="col-lg-offset-1 col-lg-2 col-md-offset-0 col-md-3 col-sm-offset-0 col-sm-3 col-xs-3">
					<img src="<?php echo get_template_directory_uri();?>/assets/images/idealRodape.png">
				</div>
				<div id="informacoesContato" class="col-lg-5 col-md-6 col-sm-7 col-xs-7">
					<span id="telefone">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/iconTelefone.png">
						<h5>(35) 3435-2603</h5>
					</span>
					<span id="email">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/iconEmail.png">
						<h5>idealhidraulica@gmail.com</h5>
					</span>
					<span id="facebook">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/iconFacebook.png">
					</span>
					<span id="local">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/iconLocal.png">
						<h5>Avenida Tancredo de Almeida Neves, 221 - Extrema/MG</h5>
					</span>
				</div>
				<div id="logoAmplia" class="col-lg-offset-1 col-lg-2 col-md-2 col-sm-offset-0 col-sm-12 col-xs-12">
					<img src="<?php echo get_template_directory_uri();?>/assets/images/logoAmplia.png">
				</div>
			<!-- <p>&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a> - <?php _e( 'All rights reserved', 'odin' ); ?> | <?php echo sprintf( __( 'Powered by the <a href="%s" rel="nofollow" target="_blank">Odin</a> forces and <a href="%s" rel="nofollow" target="_blank">WordPress</a>.', 'odin' ), 'http://wpod.in/', 'http://wordpress.org/' ); ?></p> -->
		</div><!-- .container -->
	</footer><!-- #footer -->

	<?php wp_footer(); ?>
</body>
</html>

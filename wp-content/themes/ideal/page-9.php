<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>
	</div>
	</div>
	<div class="traco" id="marginBottom">
		<div id="wrapper" class="container">
			<div class="row">
				<main id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1" role="main">
					<div id="titulo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
						<h3><?php the_title();?></h3>
					</div>
				</main>
			</div>
		</div>
	</div>
	
		<div id="wrapper" class="container">
			<div class="row">
	<main id="content" class="<?php echo odin_classes_page_full(); ?>" tabindex="-1" role="main">		
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
				?>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="pagEmpresa">
				<div class="row">
					<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12" id="paginaParceiros1">
						<h3>IDEAL SOLUÇÕES HIDRÁULICAS</h3>
						<?php the_content() ?>					
					</div>
					<div class="cycle-slideshow col-lg-offset-2 col-lg-5 col-md-offset-1 col-md-5 col-sm-12 col-xs-12" id="fotosQuemSomos"
						data-cycle-fx=scrollhorz
						data-cycle-timeout=0
						data-cycle-slides="> div"
						>
							<?php $query = new WP_Query(array(
							    'post_type' => 'fotos',
							    'posts_per_page' => 2,
							));
										if ($query->have_posts()) {
										 	while ($query->have_posts()) {
										 		$query->the_post();
										 		?>
										 		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										 		<!--  <img id="backgroundBanner" src="<?php the_field('background_imagem'); ?>" style="background-repeat:repeat-x"> -->
										 		 <?php the_post_thumbnail(); ?>
										 		</div>
										 		 <?php
										 	}
										 } 
									?>	
					</div>
				</div>
			</div>			
		</div>
		<?php
				endwhile;
			?>
	</main><!-- #main -->

<?php
get_footer();
